import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import javax.swing.JFrame;

//网页解析专用包jsoup
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
////分词器专业包IKSegmenter
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Spider {
	private static JFrame jframe = new JFrame();
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
	public static void main(String[] args) throws IOException {
		jframe.setFont(new Font("微软雅黑", Font.PLAIN, 28));
		jframe.setTitle("\u516C\u6587\u901A\u5C0F\u722C\u866B");
		jframe.setBounds(100, 100, 591, 516);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u8D77\u59CBID");
		lblNewLabel.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel.setBounds(59, 84, 66, 49);
		jframe.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(199, 92, 144, 37);
		jframe.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("\u8BF7\u8F93\u5165\u722C\u53D6\u8303\u56F4\uFF1A");
		lblNewLabel_1.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(28, 23, 149, 31);
		jframe.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u7EC8\u6B62ID");
		lblNewLabel_2.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(59, 162, 66, 49);
		jframe.getContentPane().add(lblNewLabel_2);
		
		final JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel_4.setBounds(10, 441, 167, 26);
		jframe.getContentPane().add(lblNewLabel_4);
		jframe.setVisible(true);
		textField_1 = new JTextField();
		textField_1.setBounds(199, 169, 144, 39);
		jframe.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("\u5F00\u59CB");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String base = "http://www1.szu.edu.cn/board/view.asp?id=";
				int order = 1;
				for (int id = Integer.parseInt(textField.getText()); id <= Integer.parseInt(textField_1.getText()); id++) {
					String strid = Integer.toString(id);
					String url = base + strid;//根据深大公文通URL的特点进行爬虫
					Document doc=null;
					try {
						doc = Jsoup.connect(url).get();
					} catch (IOException e1) {
						e1.printStackTrace();
					}//jsoup网页解析
					String string = doc.text().toUpperCase();//小写转大写，统一为大写
					String title = doc.title();
					if (title.equals("深圳大学内部网"))
						continue;// 如果爬到不存在的公文通，会自动跳转到title为"深圳大学内部网"的主页，这些id没有意义
					StringReader sr = new StringReader(string);
					IKSegmenter ik = new IKSegmenter(sr, true);//进行分词（核心步骤）
					Lexeme lex = null;
					String txt = "";
					try {
						while ((lex = ik.next()) != null) {
							txt += lex.getLexemeText() + "  ";//分词后以空格隔开
						}
					} catch (IOException e1) {
						// TODO 自动生成的 catch 块
						e1.printStackTrace();
					}
					FileWriter writer;
					System.out.println(order+":"+url + title);//在控制台打印出爬到的网页URL以及对应的标题
					try {
						
						String path = textField_2.getText() + order + ".txt";
						writer = new FileWriter(path);//将文本保存到指定目录
						writer.write(txt);
						writer.flush();
						writer.close();
						order++;
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(id==Integer.parseInt(textField_1.getText()))
							{
						lblNewLabel_4.setText("恭喜你，爬取完毕！");
							}
				}
			}
		});
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btnNewButton.setBounds(418, 393, 105, 49);
		jframe.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("\u8BF7\u8F93\u5165\u4FDD\u5B58\u8DEF\u5F84\uFF1A");
		lblNewLabel_3.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel_3.setBounds(28, 271, 149, 37);
		jframe.getContentPane().add(lblNewLabel_3);
		
		textField_2 = new JTextField();
		textField_2.setBounds(199, 271, 279, 37);
		jframe.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		
	}
}
