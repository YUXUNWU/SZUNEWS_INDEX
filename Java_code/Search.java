
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

//分词器专用包IKSegmenter
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

//用户交互专用包
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//
public class Search {
	private static JFrame jframe = new JFrame();
	private static Map<String, LinkedList<Integer>> map = new HashMap<>();
	private LinkedList<Integer> list;
	private Map<String, Integer> nums = new HashMap<>();
	private static JTextField textField;
	private static JTextArea downloadlist;
	private static JButton btnNewButton;

	static double TF(String Q, String D[]) {
		double documentlen = D.length;
		double frequency = 0;
		for (int i = 0; i < documentlen; i++) {

			if (Q.equals(D[i])) {
				frequency++;
				// System.out.println(frequency+"我是频率");
			}
		}
		double tf = frequency / documentlen;
		// System.out.println(tf+"我是tf");
		return tf;
	}// TF函数，计算查询词项Q在指定文档D中的频率

	static double IDF(String Q, ArrayList<String[]> D) {
		int documentnum = D.size();
		int flag = 0;
		for (int i = 0; i > documentnum; i++) {
			double tf = TF(Q, D.get(i));
			if (tf != 0)
				flag++;

		}

		double idf = Math.log(documentnum / (flag + 1));

		return idf;
	}// IDF函数，计算查询词项在整个文档集中的逆文档频率

	public void CreateIndex(String filepath, int strid_) {
		String[] words = null;
		try {

			File file = new File(filepath);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String s = null;
			while ((s = reader.readLine()) != null) {
				// 获取单词
				words = s.split(" ");

			}
			for (String string : words) {

				if (!map.containsKey(string)) {
					list = new LinkedList<Integer>();
					list.add(strid_);
					map.put(string, list);
					nums.put(string, 1);
				} else {
					list = map.get(string);
					// 如果没有包含过此文件名，则把文件名放入
					if (!list.contains(strid_)) {
						list.add(strid_);
					}
					// 文件总词频数目
					int count = nums.get(string) + 1;
					nums.put(string, count);
				}
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		jframe.setFont(new Font("微软雅黑", Font.PLAIN, 28));
		jframe.setTitle("\u516C\u6587\u901A\u4FE1\u606F\u68C0\u7D22\u7CFB\u7EDF");
		jframe.setBounds(100, 100, 591, 722);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("\u8BF7\u8F93\u5165\u641C\u7D22\u9879");
		lblNewLabel.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		lblNewLabel.setBounds(29, 49, 144, 36);
		jframe.getContentPane().add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(199, 49, 284, 36);
		jframe.getContentPane().add(textField);
		textField.setColumns(10);
		downloadlist = new JTextArea();
		downloadlist.setBackground(Color.WHITE);
		downloadlist.setForeground(Color.BLACK);
		JScrollPane list = new JScrollPane(downloadlist);
		list.setSize(555, 476);
		list.setLocation(10, 197);
		list.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jframe.getContentPane().add(list);

		btnNewButton = new JButton("\u5F00\u59CB\u641C\u7D22");
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btnNewButton.setBounds(221, 133, 110, 43);
		jframe.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				downloadlist.setText("");
				// Scanner reader = new Scanner(System.in);//输入
				// String string=reader.next();

				String string = textField.getText().toUpperCase();// 小写转大写，统一为大写

				StringReader sr = new StringReader(string);
				IKSegmenter ik = new IKSegmenter(sr, true);
				Lexeme lex = null;
				ArrayList<String> word = new ArrayList<String>();
				int num = 0;
				try {
					while ((lex = ik.next()) != null) {
						word.add(lex.getLexemeText());
						System.out.println(word.get(num));
						num++;
					}
				} catch (IOException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}

				LinkedList<Integer> l0 = map.get(word.get(0));

				boolean tip = true;
				for (int j = 0; j < num; j++) {
					if (map.containsKey(word.get(j)) == false) {
						tip = false;
						break;
					}

				}

				if (tip)// 当所有查询词项都存在的时候
				{

					for (int a = 0; a < num - 1; a++) {//两两合并
						LinkedList<Integer> l = l0;// 获取第一个单词的链表
						int size1 = l.size();// 获取链表长度1
						// System.out.println(size1+"size1");
						LinkedList<Integer> l2 = map.get(word.get(a + 1));// 获取下一个单词的链表
						int size2 = l2.size();// 获取链表长度2
						// System.out.println(size2+"size2");
						LinkedList<Integer> l1 = new LinkedList<Integer>();
						int i = 0, j = 0;
						while (i < size1 && j < size2) {
							int ai = l.get(i);
							int bi = l2.get(j);
							// System.out.println(ai+"ai");
							// System.out.println(bi+"bi");
							if (ai == bi)// 在同一个文档中
							{
								l1.add(ai);// 加入文档位置到新链表
								i++;
								j++;

							} else if (ai < bi) {
								i++;
							} else {
								j++;
							}
						}
						l0 = l1;
					}
					int sizesum1 = l0.size();
					System.out.println("符合条件的文档ID有" + sizesum1 + "个");
					downloadlist.append("符合条件的文档ID有" + sizesum1 + "个");
					System.out.println("分别为：");
					downloadlist.append("分别为：" + "\n");
					for (int t = 0; t < sizesum1; t++)
						System.out.print(l0.get(t) + "\n");// 输出合并结果
					int size = word.size();
					String[] Query = (String[]) word.toArray(new String[size]);

					ArrayList<String[]> Documentlist = new ArrayList<String[]>();

					for (int k = 0; k < sizesum1; k++) {
						String str = "";

						String path = "D:/gongwentong/" + l0.get(k) + ".txt";
						File file = new File(path);

						try {
							FileInputStream in = new FileInputStream(file);
							// size 为字串的长度 ，这里一次性读完
							int size1 = in.available();
							byte[] buffer = new byte[size1];
							in.read(buffer);
							in.close();
							str = new String(buffer, "GB2312");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String[] re = str.split(" ");// 用split()函数直接分割

						Documentlist.add(re);

					}

					// 设定给定的查询和文档集
					int querynum = Query.length;// 计算查询的term个数
					// System.out.println(querynum+"我是词数量");
					double idf[] = new double[querynum];// 创建idf数组，用来记录每个查询term的idf
					for (int i = 0; i < querynum; i++) {
						idf[i] = IDF(Query[i], Documentlist);
					} // 调用idf函数，计算每个查询term的idf
					int documentnum = Documentlist.size();// 计算文档集中文档的个数

					double scores[] = new double[documentnum];// 创建scores数组，用来记录每篇文档的相似评分
					int order[] = new int[documentnum];// 创建序号数组，用来记录每个分数对应的原始下标，因为下面排序函数会打乱序号
					System.out.println("得分情况：");
					for (int x = 0; x < documentnum; x++) {

						scores[x] = 0;
						order[x] = x;
						int y;
						for (y = 0; y < querynum; y++) {
							scores[x] += TF(Query[y], Documentlist.get(x)) * idf[y];// 核心部分：tf_idf法计算文档的相似度评分

						}
						// System.out.println(scores[x]+"我是评分");
						scores[x] = scores[x] / Documentlist.get(x).length;// 避免文档长度的影响，除以文档长度后得到最终得分

						System.out.println("第" + (x + 1) + "篇文档得分：" + scores[x]);
					}
					System.out.println("相似度排序：");
					for (int k = 0; k < documentnum; k++) {// 排序
						for (int j = k + 1; j < documentnum; j++) {
							if (scores[j] > scores[k]) {
								double x = scores[k];
								scores[k] = scores[j];
								scores[j] = x;
								int y = order[k];
								order[k] = order[j];
								order[j] = y;
							}
						}
						// System.out.println(order[k] + 1);//按评分从高到低将文档序号输出

						System.out.print(l0.get(order[k]) + "\n");// 输出合并结果
						downloadlist.append(l0.get(order[k]) + "\n");
					}
				} else {
					System.out.println("对不起，不存在符合您要求的结果！");
					downloadlist.append("对不起，不存在符合您要求的结果！");
				}
			}
		});
		jframe.setVisible(true);
		Search index = new Search();

		for (int id = 1; id <= 192; id++) {
			String strid = Integer.toString(id);
			String path = "D:/gongwentong/" + strid + ".txt";

			index.CreateIndex(path, id);
		}
		for (Entry<String, LinkedList<Integer>> map : index.map.entrySet()) {
			System.out.println(map.getKey() + ":" + map.getValue());
		}

		// for (Map.Entry<String, Integer> num : index.nums.entrySet()) {
		// System.out.println(num.getKey()+":"+num.getValue());
		// }
	}
}